import csv
import time
import urllib.request
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import re

pattern = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
blacklist = [
    "",
    ""
    ]


def clearEmail( email ):
    return email.replace('href', "").replace('mailto', "").replace('=', "").replace(':', "").replace('"', "").replace(" ", "").replace("'", "").replace(">", "").replace("/", "")

def validateEmail( email ):
    valid = True
    valid = valid and pattern.match(email)
    if not valid:
        print("Didn't match regex: " + email)
        return valid

    valid = valid and not email in blacklist
    if not valid:
        print("Email in blacklist: " + email)
        return valid

    valid = valid and not "\n" in email
    if not valid:
        print("Contains \\n: " + email)
        return valid

    valid = valid and not "}" in email
    if not valid:
        print("Contains }: " + email)
        return valid

    valid = valid and not "," in email
    if not valid:
        print("Contains ,: " + email)
        return valid

    valid = valid and not ";" in email
    if not valid:
        print("Contains ;: " + email)
        return valid

    valid = valid and not "#" in email
    if not valid:
        print("Contains #: " + email)
        return valid

    valid = valid and not ".js" in email
    if not valid:
        print("Contains .js: " + email)
        return valid

    valid = valid and not ".css" in email
    if not valid:
        print("Contains .css: " + email)
        return valid

    valid = valid and not ".png" in email
    if not valid:
        print("Contains .png: " + email)
        return valid

    valid = valid and "." in email
    if not valid:
        print("Contains .: "  + email)
        return valid
    valid = valid and len(email) < 50
    if not valid:
        print("Longer than 50: " + email)
        return valid
    return valid


opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
outfile = open('output.csv', 'w')
writer = csv.writer(outfile, delimiter=',', quotechar='"')

with open('file.csv', 'r') as csvfile:
    csvFile = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in csvFile:
        query = row[1] + " " + row[2] + " " + row[5]
        print ("Searching website for:" + query)
        try:
            email = ""
            query = { 'q' : query}
            query = urllib.parse.urlencode(query)
            with opener.open("https://www.google.be/search?q=" + query) as response:
                google = response.read()
                soup = BeautifulSoup(google, "html5lib")
                firstHit = soup.findAll('h3', class_='r')
                citeTag = soup.findAll('cite')
                foundMail = False
                for x in range(0, 5):
                    if foundMail:
                        break
                    if x != 0:
                        print("Going to next site")
                    firstA = firstHit[x].find('a', href=True)
                    domain = citeTag[x].text
                    print("Found site: " + domain)
                    if not domain.startswith( 'http' ):
                        domain = 'http://' + domain
                    domain = urlparse(domain)
                    domain = '{uri.scheme}://{uri.netloc}/'.format(uri=domain)
                    url = "https://www.google.be" + firstA['href']
                    print("Parsed site: " + domain)
                    if url:
                        with opener.open(url) as response:
                            website = response.read()
                            website = website.decode()
                            domain = domain.replace("http://","").replace("https://","").replace("www.", "").replace("/", "")
                            domain = "@" + domain
                            print("Searchin for mail-address ending with: " + domain)
                            foundMail = False
                            index = website.find(domain)
                            if(index != -1):
                                index = index + len( domain )
                                website = website[:index]
                                spaceIndex = website.rfind(" ")
                                gtIndex = website.rfind(">")
                                if(spaceIndex != -1 or gtIndex != -1):
                                    if(spaceIndex > gtIndex):
                                        index = spaceIndex
                                    else:
                                        index = gtIndex
                                    email = website[index:]
                                    email = clearEmail(email)
                                    if validateEmail(email):
                                        foundMail = True
                                        print("Found: " + email)
                            if not foundMail:
                                print("Searchin for any mail")
                                foundMail = False
                                index = website.find("@")
                                while(index != -1 and not foundMail):
                                    startSpaceIndex = website[:index].rfind(" ")
                                    startGtIndex = website[:index].rfind(">")
                                    startQuoteIndex = website[:index].rfind("\"")
                                    endSpaceIndex = website[index:].find(" ")
                                    endGtIndex = website[index:].find("<")
                                    endQmIndex = website[index:].find("?")
                                    if ((startSpaceIndex != -1 or startGtIndex != -1) and (endSpaceIndex != -1 or endGtIndex != -1 or endQmIndex != -1)):
                                        start_index = startGtIndex
                                        if(startSpaceIndex > startGtIndex):
                                            start_index = startSpaceIndex
                                        if(startQuoteIndex > start_index):
                                            start_index = startQuoteIndex
                                        end_index = endGtIndex
                                        if(endSpaceIndex < endGtIndex and endSpaceIndex != -1):
                                            end_index = endSpaceIndex
                                        if(endQmIndex < end_index and endQmIndex != -1):
                                            end_index = endQmIndex
                                        end_index = end_index + index
                                        email = website[start_index:end_index]
                                        email = clearEmail(email)
                                        if (validateEmail(email)):
                                            print("Found: " + email)
                                            foundMail = True
                                        else:
                                            index += 1
                                            subIndex = website[index:].find("@")
                                            if subIndex != -1:
                                                index = index + subIndex
                                            else:
                                                index = -1

                time.sleep(2)
        except Exception as e:
            print ("An error occurred: " + str(e))
        finally:
            if email:
                email = email.encode('latin-1', 'ignore')
                writer.writerow([row[0], row[1], row[2], row[3], row[4], row[5], row[6], email.decode("latin-1")])

outfile.close()
